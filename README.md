# GitLab Continuous Integration Setup

O GitLab nos provém com um [sistema embutido](https://about.gitlab.com/product/continuous-integration/) de CI/CD. Isto é, podemos configurar uma pipeline para builds e testes que são executados por VMs (Runners) do site.

Pode-se configurar Runners customizados executados por VMs particulares, caso seja necessário.

Os Runners utilizam-se de "imagens" do [Docker Hub](https://hub.docker.com/). Elas são pré-configuradas para possuir diferentes configurações predefinidas como, por exemplo, uma instalação de Python.

Confira a [documentação](https://docs.gitlab.com/ee/ci/quick_start/) para informações mais aprofundadas.

## Configuração

A configuração do sistema é feita por um arquivo [`.gitlab-ci.yml`](https://gitlab.com/avellar-devops/ci-setup/blob/master/.gitlab-ci.yml). Nele, podemos definir "jobs" a serem executados durante a build. Mais informações sobre o arquivo podem ser lidas [na documentação](https://docs.gitlab.com/ee/ci/yaml/README.html).

No caso desse projeto, utilizou-se código para:

- Definir um _default_ (que executa em todos os jobs se não for substituído) para imprimir a versão do Python sendo utilizada **antes** de cada job:
```yaml
default:
  before_script:
  - python --version
```

- Configurar jobs (com nome "Python X.X") para executar o arquivo `matrix.py` em cada versão de Python disponível (neste exemplo, v2.7);<br>
  Note que o campo "`image`" refere-se ao nome da imagem do Docker Hub, "[`python:2.7`](https://hub.docker.com/_/python#image-variants)"
```yaml
Python 2.7:
  image: python:2.7
  script:
  - python matrix.py
```

<br>

O arquivo `.gitlab-ci.yml` pode ser validado em \[[gitlab.com/\<nome do dono>/\<nome do repositorio>/-/ci/lint](https://gitlab.com/avellar-devops/ci-setup/-/ci/lint)\]. Nesse caso, \[gitlab.com/**avellar-devops**/**ci-setup**/-/ci/lint\]. Assim, pode-se verificar se ele é válido sem precisar de um commit que (obrigatoriamente) falha no build.


## Uso

Não é necessário fazer nada para utilizar o CI após a criação do arquivo de configuração. Ao fazer um commit, todos os jobs definidos no arquivo de configuração serão executados:

![screenshot da tela de jobs](https://i.imgur.com/to6bENX.png?1)

Cada job é uma VM rodando os comandos especificados. Pode-se ver o output de cada job em detalhes:

![screenshot de um job](https://i.imgur.com/Xl0s8wR.png?1)

Após o término do build, o commit receberá um indicador para mostrar se falhou ou não passou em testes:

![screenshot do commit](https://i.imgur.com/QdsMHHa.png?1)

Após um merge, o projeto passa por outro estágio de build para confirmar que não há nenhum erro:

![screenshot do merge](https://i.imgur.com/4o5orY5.png?1)

Por fim, o resultado do build pode ser visto no commit de merge, assim como em qualquer outro commit:

![screenshot da página do projeto](https://i.imgur.com/wWrM1JS.png?1)

<hr>

Para pular a execução de testes, pode-se adicionar `[skip ci]` à mensagem de commit. Assim, commits como edições em README e afins não precisam re-executar testes, pois não afetam código.
